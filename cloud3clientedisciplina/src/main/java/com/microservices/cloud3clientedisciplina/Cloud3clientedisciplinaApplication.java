package com.microservices.cloud3clientedisciplina;

import com.microservices.cloud3clientedisciplina.dto.Disciplina;
import com.microservices.cloud3clientedisciplina.repository.DisciplinaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

import java.util.Date;

@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
public class Cloud3clientedisciplinaApplication implements ApplicationRunner {

	@Autowired
	private DisciplinaRepository repository;


	public static void main(String[] args) {
		SpringApplication.run(Cloud3clientedisciplinaApplication.class, args);
	}

	@Override
	public void run(ApplicationArguments applicationArguments) {
		repository.save(new Disciplina(null,"MicroService", 40, new Date()));
		repository.save(new Disciplina(null,"MicroService2", 40, new Date()));
		repository.save(new Disciplina(null,"MicroService3", 40, new Date()));
		repository.save(new Disciplina(null,"MicroService4", 40, new Date()));
		repository.save(new Disciplina(null,"MicroService5", 40, new Date()));
		repository.save(new Disciplina(null,"MicroService6", 40, new Date()));
		repository.save(new Disciplina(null,"MicroService7", 40, new Date()));
		repository.save(new Disciplina(null,"MicroService8", 40, new Date()));
	}
//
    @LoadBalanced @Bean
    RestTemplate restTemplate(){
        return new RestTemplate();
    }
}
