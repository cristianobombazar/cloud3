package com.microservices.cloud3clientedisciplina.resource;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.microservices.cloud3clientedisciplina.dto.AlunoDTO;
import com.microservices.cloud3clientedisciplina.dto.Disciplina;
import com.microservices.cloud3clientedisciplina.dto.DisciplinaDTO;
import com.microservices.cloud3clientedisciplina.feign.AlunoClient;
import com.microservices.cloud3clientedisciplina.repository.DisciplinaRepository;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.*;
import java.util.stream.Collectors;

@RequestMapping("/disciplinas")
@RestController
public class DisciplinaResource {

    @Autowired
    private DisciplinaRepository repository;

//    @Autowired
//    private RestTemplate restTemplate;

    @Autowired
    private AlunoClient client;

    @GetMapping("/nomes")
    public List<String> getDisciplinas(){
        return repository.findAll().stream().map(foo -> foo.getNome()).collect(Collectors.toList());
    }

    @GetMapping("/{id}/dto")
    public DisciplinaDTO getDisciplina(@PathVariable Long id) throws Exception {
        //ResponseEntity<List> alunos = restTemplate.getForEntity("http://aluno-server/alunos/nomes", List.class);
        Resources<AlunoDTO> alunos = client.getAllAlunos();
        List<String> alunosNomes = alunos.getContent().stream().map(a -> a.getNome()).collect(Collectors.toList());
        Disciplina disciplina = repository.findOne(id);

        return DisciplinaDTO.builder()
                .id(disciplina.getId())
                .nome(disciplina.getNome())
                .cargaHoraria(disciplina.getCargaHoraria())
                .dataInicio(disciplina.getDataInicio())
                .alunosMatriculados(alunosNomes)
                .build();
    }


    //public static final String ALUNO_SERVICE = "aluno-service";

//    private DiscoveryClient discoveryClient;
//    private ObjectMapper objectMapper;


//    @Autowired
//    public DisciplinaResource(DiscoveryClient discoveryClient, ObjectMapper objectMapper){
//        this.discoveryClient = discoveryClient;
//        this.objectMapper   = objectMapper;
//    }

//    @GetMapping
//    public DisciplinaDTO findDisciplina() throws Exception{
//        List<ServiceInstance> instances = discoveryClient.getInstances(DisciplinaResource.ALUNO_SERVICE);
//        String uri = null;
//        if (instances != null & !instances.isEmpty()){
//            ServiceInstance serviceInstance = instances.get(0);
//            uri = "http://"+serviceInstance.getHost()+":"+serviceInstance.getPort();
//        }
//        if (uri != null){
//            //CRIA A REQUISIÇÃO PARA O SERVIÇO DE ALUNOS.
//            HttpClient httpClient = HttpClientBuilder.create().build();
//            HttpResponse httpResponse = httpClient.execute(new HttpGet(uri+"/alunos"));
//
//            //COMO NÃO TEMOS A CLASSE "ALUNOS", TRANSFORMAMOS EM UM MAPA PARA PEGAR A PROPRIEDADE "NOME" DEPOIS.
//            Map mapa = objectMapper.readValue(EntityUtils.toString(httpResponse.getEntity()), HashMap.class);
//
//            Map mapaEmbedded = (Map) mapa.get("_embedded"); //ver com zanivam se dá pra trocar o nome do embedded
//            List<Map> listAlunos = (List<Map>) mapaEmbedded.get("alunos"); //retorna uma lista de mapas. Cada mapa, vem com uma propriedade.
//            List<String> listaNomes = listAlunos.stream().map( foo -> (String) foo.get("nome") ).collect(Collectors.toList());
//            return DisciplinaDTO.builder()
//                                .nome("Microserviços")
//                                .cargaHoraria(30)
//                                .dataInicio(new Date())
//                                .alunosMatriculados(listaNomes)
//                                .build();
//        }else{
//            return null;
//        }
//    }
}
