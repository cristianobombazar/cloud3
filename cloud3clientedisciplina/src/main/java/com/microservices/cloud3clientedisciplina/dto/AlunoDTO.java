package com.microservices.cloud3clientedisciplina.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AlunoDTO {

    private Long id;
    String nome;
    String email;
    Integer matricula;
    List<String> disciplinas;

}

