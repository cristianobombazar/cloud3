package com.microservices.cloud3clientealuno.resource;

import com.microservices.cloud3clientealuno.RibbonConfiguration;
import com.microservices.cloud3clientealuno.feign.DisciplinaClient;
import com.microservices.cloud3clientealuno.model.Aluno;
import com.microservices.cloud3clientealuno.model.AlunoDTO;
import com.microservices.cloud3clientealuno.model.DisciplinaDTO;
import com.microservices.cloud3clientealuno.repository.AlunoRepository;
import com.netflix.discovery.converters.Auto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.xml.ws.Response;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/alunos")
@RibbonClient(name = "disciplina-service", configuration = RibbonConfiguration.class)
public class AlunoResource {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private AlunoRepository repository;

    @Autowired
    private DisciplinaClient client;

    @GetMapping("/nomes")
    public List<String> getAlunos(){
        return repository.findAll().stream().map(foo -> foo.getNome()).collect(Collectors.toList());
    }

    @GetMapping("/{id}/dto")
    @SuppressWarnings("all")
    public AlunoDTO getAluno(@PathVariable Long id) throws Exception {

        Resources<DisciplinaDTO> disciplinas = client.getAllDisciplinas();
        List<String> nomesDisciplinas = disciplinas.getContent().stream()
                .map(d -> d.getNome()).collect(Collectors.toList());

        Aluno aluno = repository.findOne(id);

        return AlunoDTO.builder().id(aluno.getId())
                .nome(aluno.getNome())
                .matricula(aluno.getMatricula())
                .email(aluno.getEmail())
                .disciplinas(nomesDisciplinas)
                .build();
    }
}
