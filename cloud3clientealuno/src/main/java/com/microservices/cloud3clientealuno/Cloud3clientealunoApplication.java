package com.microservices.cloud3clientealuno;

import com.microservices.cloud3clientealuno.model.Aluno;
import com.microservices.cloud3clientealuno.repository.AlunoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
public class Cloud3clientealunoApplication implements ApplicationRunner {

	@Autowired
	private AlunoRepository alunoRepository;

	public static void main(String[] args) {
		SpringApplication.run(Cloud3clientealunoApplication.class, args);
	}

	@Override
	public void run(ApplicationArguments applicationArguments) {


		alunoRepository.save(new Aluno(null, "Cristiano", 1, "cristiano.bombazar@outlook.com"));
		alunoRepository.save(new Aluno(null, "Cristiano2", 1, "cristiano.bombazar@outlook.com"));
		alunoRepository.save(new Aluno(null, "Cristiano3", 1, "cristiano.bombazar@outlook.com"));
		alunoRepository.save(new Aluno(null, "Cristiano4", 1, "cristiano.bombazar@outlook.com"));
		alunoRepository.save(new Aluno(null, "Cristiano5", 1, "cristiano.bombazar@outlook.com"));
		alunoRepository.save(new Aluno(null, "Cristiano6", 1, "cristiano.bombazar@outlook.com"));
		alunoRepository.save(new Aluno(null, "Cristiano7", 1, "cristiano.bombazar@outlook.com"));
	}

	@LoadBalanced
	@Bean
	RestTemplate restTemplate(){
		return new RestTemplate();
	}
}
