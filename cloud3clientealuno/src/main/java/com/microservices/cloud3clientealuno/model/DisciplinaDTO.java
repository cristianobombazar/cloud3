package com.microservices.cloud3clientealuno.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DisciplinaDTO {

    private Long id;
    private String nome;
    private Integer cargaHoraria;
    private Date dataInicio;
    List<String> alunosMatriculados;
}
