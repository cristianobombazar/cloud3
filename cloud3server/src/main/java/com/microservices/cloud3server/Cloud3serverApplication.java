package com.microservices.cloud3server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class Cloud3serverApplication {

	public static void main(String[] args) {
		SpringApplication.run(Cloud3serverApplication.class, args);
	}
}
